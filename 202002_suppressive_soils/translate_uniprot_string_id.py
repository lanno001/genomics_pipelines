import urllib.parse
import urllib.request
import pandas as pd
from io import StringIO

url = 'https://www.uniprot.org/uploadlists/'

params = {
'from': 'ID',
'to': 'GENENAME',
'format': 'tab',
'query': 'Q9ADP8 Q70IY1 P06986 P39630 P40806 B5UAT8 Q8KWT4'
}

data = urllib.parse.urlencode(params)
data = data.encode('utf-8')
req = urllib.request.Request(url, data)
with urllib.request.urlopen(req) as f:
   response = f.read()
print(response.decode('utf-8'))
df = pd.read_csv(StringIO(response.decode('utf-8')), sep='\t', header=0)
print(df.To)