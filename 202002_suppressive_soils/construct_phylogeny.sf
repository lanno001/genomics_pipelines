import os, gzip, re
from os.path import basename, splitext
from glob import glob
from Bio import SeqIO

# Generate an alignment-based tree

def parse_input_dir(in_dir, pattern=None):
    if type(in_dir) != list: in_dir = [in_dir]
    out_list = []
    for ind in in_dir:
        if not os.path.exists(ind):
            raise ValueError(f'{ind} does not exist')
        if os.path.isdir(ind):
            ind = os.path.abspath(ind)
            if pattern is None: out_list.extend(glob(f'{ind}/**/*', recursive=True))
            else: out_list.extend(glob(f'{ind}/**/{pattern}', recursive=True))
        else:
            if pattern is None: out_list.append(ind)
            elif pattern.strip('*') in ind: out_list.append(ind)
    return out_list

assemblies_dir = config['assemblies_dir']
if assemblies_dir[-1] != '/': assemblies_dir += '/'
assemblies_fn_list = parse_input_dir(assemblies_dir, pattern='*.fna.gz')
assemblies_list = [splitext(splitext(basename(ass))[0])[0] for ass in assemblies_fn_list]
outgroup = config['outgroup']
nb_cores=16


rule target:
    input:
        newick='raxml_roary_renamed.newick'

rule prokka:
    input:
        fasta=f'{assemblies_dir}{{prefix}}.fna.gz'
    output:
        gff='{prefix}.gff'
    shell:
        '''
        bn=$( basename {input.fasta} )
        wd="${{bn%.*.*}}"
        mkdir -p "${{wd}}" && cd "${{wd}}"
        gunzip -c ../{input.fasta} > temp.fasta
        prokka --force --outdir prokka_{wildcards.prefix} --genus Streptomyces --kingdom Bacteria --usegenus temp.fasta
        cp prokka_{wildcards.prefix}/PROKKA_*.gff ../{output.gff}
        '''

rule roary:
    input:
        gffs=expand('{ass}.gff', ass=assemblies_list)
    threads: nb_cores
    output:
        msa='roary.msa'
    shell:
        '''
        roary -f roary_wd -e -n -p {threads} {input.gffs}
        cp roary_wd/core_gene_alignment.aln {output.msa}
        '''

rule msa2phylip:
    input:
         msa='{prefix}.msa'
    output:
          phylip='{prefix}.phylip'
    run:
        records = SeqIO.parse(input.msa, "fasta")
        count = SeqIO.write(records, output.phylip, "phylip-relaxed")

rule raxml:
    input:
         phylip='roary.phylip'
    params:
        outgroup=outgroup
    output:
         newick='raxml_roary.newick'
    shell:
         """
         mkdir raxml && cd raxml
         raxmlHPC -f a -m GTRGAMMA -p 42 -x 42 -#100 -s ../{input.phylip} -n T1 -o {params.outgroup}
         cd ..
         cp raxml/RAxML_bipartitionsBranchLabels.T1 {output.newick}
         """

rule rename_tree:
    input:
        newick='raxml_roary.newick'
    output:
        newick='raxml_roary_renamed.newick'
    run:
        assemblies_list = [x.replace('.fna.gz', '') for x in os.listdir(assemblies_dir)]
        with open(input.newick, 'r') as fh: newick_txt = fh.read()
        for ass in assemblies_list:
            with gzip.open(f'{assemblies_dir}{ass}.fna.gz', 'rb') as fh: header = fh.readline().decode('utf-8')
            try:
                species = re.search('(?<=\s).+(?=,)', header).group(0)
            except AttributeError:
                print(f'Header for {ass} does not adhere to expected pattern (>id species name, xxx), keep id')
                species = ass
            newick_txt = newick_txt.replace(ass, species)
        with open(output.newick, 'w') as fh: fh.write(newick_txt)
