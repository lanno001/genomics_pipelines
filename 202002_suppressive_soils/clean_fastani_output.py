import re
import sys
import pandas as pd
from Bio import Entrez

Entrez.email = 'carlos.delannoy@wur.nl'

in_file = sys.argv[1]
out_file = sys.argv[2]
out_itol = sys.argv[3]

df = pd.read_csv(in_file, sep='\t', header=None, names=['a1', 'a2', 's1', 's2', 's3'])
df.loc[:, 'a1'] = df.a1.apply(lambda x: re.search('[^_]+_[^_]+(?=_|$)', re.search('(?<=/)[^/]+(?=\.fna\.gz)', x).group(0)).group(0))
df.loc[:, 'a2'] = df.a2.apply(lambda x: re.search('[^_]+_[^_]+(?=_|$)', re.search('(?<=/)[^/]+(?=\.fna\.gz)', x).group(0)).group(0))
df.to_csv(out_file, sep='\t', header=False, index=False)

assembly_list = list(df.a1.unique())

itol_df = pd.DataFrame(columns=['tax_id', 'species'], index=assembly_list)

for ass in assembly_list:
    try:
        ass_id = Entrez.read(Entrez.esearch(db='assembly', term=ass))['IdList'][0]
        tax_id = Entrez.read(Entrez.elink(dbfrom='assembly', id=ass_id, db='taxonomy'))[0]['LinkSetDb'][0]['Link'][0]['Id']
        with Entrez.efetch(db='taxonomy', id=tax_id, retmode='xml') as fh:
            sname = Entrez.read(fh)[0]['ScientificName']
        itol_df.loc[ass, 'tax_id'] = int(tax_id)
        itol_df.loc[ass, 'species'] = sname
    except:
        print(f'no match for {ass}')
        itol_df.loc[ass, 'tax_id'] = None
        itol_df.loc[ass, 'species'] = ass

itol_str = 'LABELS\nSEPARATOR TAB\nDATASET_LABEL\ttaxonomy\nCOLOR\t#ff0000\nDATA\n' + itol_df.loc[:, 'species'].to_csv(None, sep='\t', header=False, index=True).replace('\n', '\t-1\t#000000\tnormal\t1\n')
with open(out_itol, 'w') as fh: fh.write(itol_str)
