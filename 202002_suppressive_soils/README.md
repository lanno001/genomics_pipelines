# Genome, metabolome and antifungal activities of Streptomyces from disease suppressive soils

This repository contains supplementary data and bioinformatic workflows 
used in the data analysis for the following publication:



### construct_phylogeny.sf
Construct an alignment-based phylogeny. First use prokka/roary to generate
the required core genome alignment, then use RAxML to construct bootstrapped
trees and ultimately a consensus tree.
```
snakemake --snakefile construct_phylogeny.sf --config \
    assembly_dir=dir/containing/assemblies \
    outgroup=outgroup_name 
```

### gene_finding.sf
Perform structural variant detection using paftools, gene finding and 
diffing using prokka/roary.