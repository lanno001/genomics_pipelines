from io import StringIO
import sys
import pandas as pd
import re
import numpy as np


def parse_prokka_gff(gff_fn, colnums, colnames):
    """
    :param gff_fn:
    :param colnums:
    :param colnames:
    :return:
    """
    gff_lines = []
    with open(gff_fn, 'r') as fh:
        line = True
        while line:
            line = fh.readline()
            if '##FASTA' in line: break
            elif '##' in line: continue
            elif 'hypothetical protein' in line: continue
            gff_lines.append(line)
    gff_txt = StringIO('\n'.join(gff_lines))
    gff_df = pd.read_csv(gff_txt, sep='\t', usecols=colnums, names=colnames)
    return gff_df

ref_gff = sys.argv[1]
query_gff = sys.argv[2]


fn_dict = {'ref': ref_gff, 'query': query_gff}
df_dict = {}
for fn in fn_dict:
    df_dict[fn] = parse_prokka_gff(fn_dict[fn], [3, 4, 6, 8], ['start', 'end', 'orientation', 'gene_txt'])

# parse gene names
gene_df_dict = {}
for df_fn in df_dict:
    df = df_dict[df_fn]
    df.loc[:, 'gene'] = pd.Series('', dtype='string')
    df.loc[:, 'UniProtKB'] = pd.Series('', dtype='string')
    df.loc[:, 'product'] = pd.Series('', dtype='string')
    for ti, tup in df.iterrows():
        if 'Name' in tup.gene_txt: df.loc[ti, 'gene'] = re.search('(?<=Name=)[^;]+(?=;)', tup.gene_txt).group(0)
        if 'UniProtKB' in tup.gene_txt: df.loc[ti, 'UniProtKB'] = re.search('(?<=UniProtKB:)[^;]+(?=;)', tup.gene_txt).group(0)
        if 'product' in tup.gene_txt: df.loc[ti, 'product'] = re.search('(?<=product=)[^;]+(?=;|$)', tup.gene_txt).group(0)
    # df.drop(['gene_txt'], axis=1, inplace=True)
    df_dict[df_fn] = df.reindex(['gene', 'UniProtKB', 'product', 'start', 'end', 'orientation', 'gene_txt'], axis=1)

# Diff both ways using as identifier:
#  - gene name, or if not available...
#  - uniprotKB ID, or if not available...
#  - product
gene_ref_bool = ~df_dict['ref'].gene.isna(); gene_query_bool = ~df_dict['query'].gene.isna()
up_ref_bool = np.logical_and(~df_dict['ref'].UniProtKB.isna() , ~gene_ref_bool)
up_query_bool = np.logical_and(~df_dict['query'].UniProtKB.isna(), ~gene_query_bool)
product_ref_bool = np.logical_and(~df_dict['ref'].loc[:, 'product'].isna(), ~up_ref_bool)
product_query_bool = np.logical_and(~df_dict['query'].loc[:, 'product'].isna(), ~up_query_bool)
diff_bool_dict = {'gene': [gene_ref_bool, gene_query_bool],
                  'UniProtKB': [up_ref_bool, up_query_bool],
                  'product': [product_ref_bool, product_query_bool]}

del_list = []
ins_list = []
for dc in diff_bool_dict:
    gene_diff_df = df_dict['ref'].loc[diff_bool_dict[dc][0], :].join(df_dict['query'].loc[diff_bool_dict[dc][1], :].set_index(dc),
                                                             on=dc, how='outer', lsuffix='_ref', rsuffix='_query')
    gene_diff_df.loc[:, dc + '_ref'] = gene_diff_df.loc[:, dc]
    gene_diff_df.loc[:, dc + '_query'] = gene_diff_df.loc[:, dc]
    del_list.append(gene_diff_df.loc[gene_diff_df.gene_txt_query.isna(), :].reindex(['gene', 'UniProtKB', 'product', 'start_ref', 'end_ref', 'orientation_ref', 'gene_txt_ref'], axis=1))
    ins_list.append(gene_diff_df.loc[gene_diff_df.gene_txt_ref.isna(), :].reindex(['gene', 'UniProtKB', 'product','start_query', 'end_query', 'orientation_query', 'gene_txt_query'], axis=1))
del_df = pd.concat(del_list).reset_index().drop('index', axis=1)
ins_df = pd.concat(ins_list).reset_index().drop('index', axis=1)

del_df.to_csv('somepath', sep='\t', index=False, header=True)
ins_df.to_csv('somepath', sep='\t', index=False, header=True)