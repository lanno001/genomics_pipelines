import sys
import pandas as pd
from io import StringIO
import re


prokka_gff = sys.argv[1]
out_fn = sys.argv[2]
cp=1

gff_lines = []
with open(prokka_gff, 'r') as fh:
    line = True
    while line:
        line = fh.readline()
        if '##FASTA' in line: break
        elif '##' in line: continue
        elif 'hypothetical protein' in line: continue
        gff_lines.append(line)
gff_txt = StringIO('\n'.join(gff_lines))
gff_df = pd.read_csv(gff_txt, sep='\t', usecols=[0,3,4,6,8], names=['insertion', 'start', 'end', 'orientation', 'gene_txt'])
gff_df.loc[:, 'contig'] = ''
gff_df.loc[:, 'ref_position'] = ''
gff_df.loc[:, 'gene'] = ''
gff_df.loc[:, 'UniProtKB'] = ''
for ti, tup in gff_df.iterrows():
    gff_df.loc[ti, 'contig'], _, gff_df.loc[ti, 'ref_position'] = tup.insertion.rpartition('_')
    if 'Name' in tup.gene_txt: gff_df.loc[ti, 'gene'] = re.search('(?<=Name=)[^;]+(?=;)', tup.gene_txt).group(0)
    if 'UniProtKB' in tup.gene_txt: gff_df.loc[ti, 'UniProtKB'] = re.search('(?<=UniProtKB:)[^;]+(?=;)', tup.gene_txt).group(0)
gff_df.drop(['gene_txt', 'insertion'], axis=1, inplace=True)
gff_df = gff_df.reindex(['contig', 'ref_position', 'gene', 'UniProtKB', 'start', 'end', 'orientation'], axis=1)
gff_df.to_csv(out_fn, sep='\t', index=False, header=True)
