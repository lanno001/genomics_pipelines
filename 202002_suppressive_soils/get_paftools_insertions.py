import sys
import pandas as pd
import numpy as np
from io import StringIO


paftools_fn = sys.argv[1]
out_fn = sys.argv[2]
cp=1

with open(paftools_fn, 'r') as fh:
    paf_lines = fh.readlines()
paf_lines = [pl for pl in paf_lines if pl[0] == 'V']
paf_txt = StringIO('\n'.join(paf_lines))
paf_df = pd.read_csv(paf_txt, sep='\t', names=['V', 'contig', 'ref_start', 'ref_end', 'query_depth', 'mapping_quality', 'ref',
                                      'alt', 'query_name', 'query_start', 'query_end', 'query_orientation'])

ins_df = paf_df.loc[paf_df.apply(lambda x: x.ref_start == x.ref_end, axis=1),:]
ins_bool = np.logical_and(paf_df.query_end - paf_df.query_start > 800, paf_df.ref_start == paf_df.ref_end)
ins_df = paf_df.loc[ins_bool, :]

ins_list = []

for it, ins in ins_df.iterrows():
    ins_list.append(f'>insertion_{ins.contig}_{ins.ref_start}\n{ins.alt}')

with open(out_fn, 'w') as fh:
    fh.write('\n'.join(ins_list))
