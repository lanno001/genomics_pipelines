import sys
import re
import pandas as pd
from Bio import SeqIO, Entrez
import argparse
from io import StringIO

Entrez.email = 'carlos.delannoy@wur.nl'

# filter barrnap output, retain only 16S sequences, generate iTOL style txt

parser = argparse.ArgumentParser()
parser.add_argument('--in-fasta', type=str,required=True)
parser.add_argument('--out-fasta', type=str,required=True)
parser.add_argument('--out-itol', type=str,required=True)
parser.add_argument('--barrnap', action='store_true',required=True)
args = parser.parse_args()
in_fasta = args.in_fasta
out_itol = args.out_itol
out_fasta = args.out_fasta

itol_df = pd.DataFrame(columns=['gb_id', 'tax_id', 'species']).set_index('gb_id')
assembly_list = []
with open(in_fasta, 'r') as fi, open(out_fasta, 'w') as fo:
    for fa in SeqIO.parse(fi, 'fasta'):
        if args.barrnap:
            if '16S_rRNA' not in fa.name: continue
            assembly = re.search('(?<=:)[^:]+(?=:)', fa.name).group(0)
        else:
            assembly = fa.name
        if assembly in assembly_list: continue
        assembly_list.append(assembly)
        fa.id = assembly
        fa.description = ''
        SeqIO.write(fa, fo, 'fasta')

itol_df = pd.DataFrame(columns=['tax_id', 'species'], index=assembly_list)
dblink_list = []
for ass in assembly_list:
    try:
        tax_id = Entrez.read(Entrez.elink(dbfrom='nuccore', id=ass, db='taxonomy'))[0]['LinkSetDb'][0]['Link'][0]['Id']
        with Entrez.efetch(db='taxonomy', id=tax_id, retmode='xml') as fh:
            sname = Entrez.read(fh)[0]['ScientificName']
        itol_df.loc[ass, 'tax_id'] = int(tax_id)
        itol_df.loc[ass, 'species'] = sname
    except:
        print(f'no match for {ass}')
        itol_df.loc[ass, 'tax_id'] = None
        itol_df.loc[ass, 'species'] = 'unknown'

itol_str = 'LABELS\nSEPARATOR TAB\nDATASET_LABEL\ttaxonomy\nCOLOR\t#ff0000\nDATA\n' + itol_df.loc[:, 'species'].to_csv(None, sep='\t', header=False, index=True).replace('\n', '\t-1\t#000000\tnormal\t1\n')

with open(out_itol, 'w') as fh: fh.write(itol_str)

    # for line in fi.readlines():
    #     if line[0] == '>':
    #         assembly = re.search('(?<=:)[^:]+(?=:)', line).group(0)
    #         assembly_nb = int(assembly_dict.get(assembly, - 1) + 1)
    #         assembly_dict[assembly] = assembly_nb
    #         skip = False
    #         if assembly_nb > 0:
    #             assembly += f'_{assembly_nb}'
    #         line = '>' + assembly + '\n'
    #     if not skip:
    #         fo.write(line)
