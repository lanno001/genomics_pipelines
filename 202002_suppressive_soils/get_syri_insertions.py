import sys
import pandas as pd
import numpy as np
from io import StringIO


syri_fn = sys.argv[1]
out_fn = sys.argv[2]
cp=1

with open(syri_fn, 'r') as fh: syri_lines = fh.readlines()
syri_lines = [pl for pl in syri_lines if '<INS>' in pl]
syri_txt = StringIO('\n'.join(syri_lines))
syri_df = pd.read_csv(syri_txt, sep='\t', names=['CHROM','POS','ID','REF','ALT','QUAL','FILTER','INFO'])

cp=1

