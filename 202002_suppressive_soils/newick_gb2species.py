from sys import argv
from ete3 import NCBITaxa

# convert a newick tree from containing ncbi taxonomy names to one species names

ncbi = NCBITaxa()

with open(argv[1], 'r') as pnh, open(argv[2], 'w') as pih, open(argv[3], 'w') as lih:
    for line in pnh:
        if ':' in line:
            tax_name = line.split(':')[0]
            tax_name_clean = tax_name.lstrip(')').strip('\'')
            tax_id_dict = ncbi.get_name_translator([tax_name_clean])
            if not len(tax_id_dict):
                raise ValueError(f'No taxid found for {tax_name_clean}')
            tax_id = tax_id_dict[tax_name_clean][0]
            line = line.replace(tax_name.strip(')'), str(tax_id))
            lih.write(f'{str(tax_id)}\n')
        pih.write(line)
