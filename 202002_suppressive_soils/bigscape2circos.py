import argparse, os, pickle, re, json
import pandas as pd
from jinja2 import Template
from glob import glob
from tempfile import TemporaryDirectory
from itertools import combinations
import numpy as np
from math import ceil

def find(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)
    raise ValueError(f'Cannot find {name}')

def parse_input_dir(in_dir, pattern=None):
    if type(in_dir) != list: in_dir = [in_dir]
    out_list = []
    for ind in in_dir:
        if not os.path.exists(ind):
            raise ValueError(f'{ind} does not exist')
        if os.path.isdir(ind):
            ind = os.path.abspath(ind)
            if pattern is None: out_list.extend(glob(f'{ind}/**/*', recursive=True))
            else: out_list.extend(glob(f'{ind}/**/{pattern}', recursive=True))
        else:
            if pattern is None: out_list.append(ind)
            elif pattern.strip('*') in ind: out_list.append(ind)
    return out_list

def as_dict2df(as_dict, adf):
    prot_dict = {}
    for rec in as_dict['records']:
        if not 'antismash.modules.clusterblast' in rec['modules']: continue
        for cnb, cluster in enumerate(rec['modules']['antismash.modules.clusterblast']['knowncluster']['results']):
            if not len(cluster['ranking']): continue
            cluster_id = f'{rec["id"]}.region{str(cnb+1).zfill(3)}'
            if cluster_id not in adf.index: continue
            adf.loc[cluster_id, 'closest_bgc'] = cluster['ranking'][0][0]['description']
            prot_dict[cluster_id] = cluster['ranking'][0][0]['proteins']
    return prot_dict

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

with open(f'{__location__}/circos_template.conf', 'r') as fh: circos_template = Template(fh.read())
with open(f'{__location__}/ideogram_template.conf', 'r') as fh: ideogram_template = Template(fh.read())

color_dict = {
    'Terpene': 'yellow',
    'NRPS': 'blue',
    'Others': 'grey',
    'PKS-NRP_Hybrids': 'dpurple',
    'PKSI': 'dred',
    'RiPPs': 'green',
    'PKSother': 'vlred'
}

# glyph character mappings
#
# small
# | medium
# | | large
# | | |
# a b c   square
# d e f   rhombus
# g h i   triangle up
# j k l   triangle down
# m n o   circle
#
# lower case - hollow
# upper case - solid

symbol_dict = {it: sym for it, sym in enumerate(['■', '▲', '●', '★'])}

def get_link_color(fn, shared_group):
    if 'PKSI' in fn and shared_group != 'T1PKS':
        return 'grey'
    return [color_dict[cd] for cd in color_dict if cd in fn][0]

parser = argparse.ArgumentParser(description='Make circos graph of BiG-SCAPE results')
parser.add_argument('--bigscape-dir', type=str, help='BiG-SCAPE output directory')
parser.add_argument('--sort-by-type', action='store_true',
                    help='Sort BGCs per type rather than genome index.')
parser.add_argument('--distance-limit', type=float, default=0.1,
                    help='raw distance above which ribbons are drawn translucent. [default: 0.1]')
parser.add_argument('--out-svg', type=str, help='circos plot svg')
parser.add_argument('--acc-regex', type=str, required=False,
                    help='regex pattern for accessions that should be of the same kind (e.g. contigs of same assembly).')
parser.add_argument('--place-marker-regex', type=str, required=False, nargs='+',
                    help='Place a marker at BGCs that fit a certain regex. '
                         'No more than 4. If multiple fit only first in list is displayed.')
parser.add_argument('--antismash-dirs', type=str, required=False, nargs='+',
                    help='optionally, add antismash directories to parse out most similar cluster names.')
parser.add_argument('--link-by-closest-bgc', action='store_true',
                    help='Links signify that clusters had the same closest cluster according to antiSMASH, rather'
                         'than a link found by BiG-SCAPE.')
parser.add_argument('--network-cutoff', type=float, default=0.3,
                    help='raw distance cutoff that bigscape maintained to establish links')

args = parser.parse_args()

annotations_fn = find('Network_Annotations_Full.tsv', args.bigscape_dir)
annotations_df = pd.read_csv(annotations_fn, sep='\t').set_index('BGC')  # cols: Accession ID, Description, Product_prediction, BiG-SCAPE class, organism, Taxonomy
annotations_df.loc[:, 'bgc_nb'] = annotations_df.apply(lambda x: int(re.search('(?<=region)[0-9]+', x.name).group(0)), axis=1)
annotations_df.loc[:, 'symbol'] = None
annotations_df.loc[:, 'closest_bgc'] = ''
if args.acc_regex:
    annotations_df.loc[:, 'acc'] = annotations_df.apply(lambda x: re.search(args.acc_regex, x['Accesion ID']).group(0), axis=1)
else:
    annotations_df.loc[:, 'acc'] = annotations_df.loc[:, 'Accesion ID']
network_fn_list = parse_input_dir(args.bigscape_dir, pattern=f'*{"{0:.2f}".format(args.network_cutoff)}.network')
out_svg = os.path.realpath(args.out_svg)

# -- if markers need be placed, pull required data from antismash files ---
if args.place_marker_regex:
    prot_dict = {}
    for adir in args.antismash_dirs:
        if adir[-1] != '/': adir += '/'
        json_fn = [ad for ad in os.listdir(adir) if ad.endswith('.json')][0]
        with open(f'{adir}{json_fn}', 'r') as fh: as_dict = json.load(fh)
        prot_dict.update(as_dict2df(as_dict, annotations_df))

    # --- marker symbols ---
    annotations_df.loc[:, 'symbol'] = annotations_df.loc[:, 'closest_bgc'].apply(
        lambda x: [symbol_dict[mri] for mri, mr in enumerate(args.place_marker_regex) if
                   re.search(mr, x) is not None]).apply(lambda x: None if not len(x) else x[0])



bgc_dict_fn = find('BGCs.dict', args.bigscape_dir)
with open(bgc_dict_fn, 'rb') as fh: bgc_dict = pickle.load(fh)


# --- make karyotype ---
# chr_list = []
band_list = []
# accessions = annotations_df.loc[:, 'Accesion ID'].unique()
bgc_loc_dict = {}
chr_dict = {}
tick_dict = {}
acc_idx = 0
for acc_id, acc_df in annotations_df.groupby('Accesion ID'):
    if args.acc_regex:
        re_obj = re.search(args.acc_regex, acc_id)
        if re_obj is not None: acc_id = re_obj.group(0)
    acc_id = acc_id.replace('_', '__')
    band_pos = chr_dict.get(acc_id, 0)
    start_idx_list = []

    if args.sort_by_type:
        acc_df = acc_df.sort_values('BiG-SCAPE class')
    for bgc_name, bgc_tup in acc_df.iterrows():
        pf_dict = bgc_dict[bgc_name]
        col = color_dict[bgc_tup.loc['BiG-SCAPE class']]
        bgc_loc_dict[bgc_name] = (acc_id, band_pos, band_pos + len(pf_dict))
        band_list.append(f'band {acc_id} {bgc_name} {bgc_name} {band_pos} {band_pos + len(pf_dict)} {col}')
        nb_genes = len(pf_dict)
        if bgc_tup.symbol is not None:
            tick_dict_acc = tick_dict.get(bgc_tup.symbol, {})
            tick_dict_acc[acc_idx] = tick_dict_acc.get(acc_idx, []) + [str(int(band_pos + ceil(0.5 * nb_genes)))]
            tick_dict[bgc_tup.symbol] = tick_dict_acc
            # tick_dict[bgc_tup.symbol] = tick_dict.get(bgc_tup.symbol, []) + [str(int(band_pos + 0.5 * nb_genes))+'u']
        band_pos += len(pf_dict)
    chr_dict[acc_id] = band_pos
    acc_idx += 1
chr_list = [f'chr - {acc_id} {acc_id} 0 {chr_dict[acc_id]} black' for acc_id in chr_dict]
accessions = list(chr_dict)
karyogram_txt = '\n'.join(chr_list + band_list)

# --- get links ---
link_list = []

if args.link_by_closest_bgc:
    for comp, acc_df in annotations_df.groupby('closest_bgc'):
        if not len(comp): continue
        acc_class = acc_df.loc[:, 'BiG-SCAPE class'].unique()[0]
        col = get_link_color(acc_class, acc_class)
        for cl_name1, cl_name2 in combinations(acc_df.index, 2):
            prot1 = prot_dict[cl_name1]; prot2 = prot_dict[cl_name2]
            eq = np.mean([1 if p1 in prot2 else 0 for p1 in prot1])
            if eq < 1.0: continue
            intensity = '' if eq > 0.8 else '_a4'
            id1, s1, e1 = bgc_loc_dict[cl_name1]
            id2, s2, e2 = bgc_loc_dict[cl_name2]
            link_list.append(f'{id1} {s1} {e1} {id2} {s2} {e2} color={col}{intensity}')
else:
    for nfn in network_fn_list:
        net_df = pd.read_csv(nfn, sep='\t')
        nfn_base = os.path.basename(nfn)
        col = [color_dict[cd] for cd in color_dict if cd in nfn_base][0]
        for _, tup in net_df.iterrows():
            col = get_link_color(nfn_base, tup.loc['Shared group'])
            cl_name1, cl_name2 = tup.loc['Clustername 1'], tup.loc['Clustername 2']
            id1, s1, e1 = bgc_loc_dict[cl_name1]
            id2, s2, e2 = bgc_loc_dict[cl_name2]
            if id1 == id2: continue
            intensity = '' if tup.loc['Raw distance'] < args.distance_limit else '_a4'
            link_list.append(f'{id1} {s1} {e1} {id2} {s2} {e2} color={col}{intensity}')

# --- marker symbol text ---
tick_txt_list = []
for tick in tick_dict:
    for acc_idx in tick_dict[tick]:
        tick_txt_list.append(f'''
<tick>
chromosomes_display_default = no
position    = {','.join(tick_dict[tick][acc_idx])}
label       = {tick}
chromosomes = {accessions[acc_idx]}
color       = black
label_font  = glyph
size        = 0
thickness   = 0
show_label  = yes
label_size  = 30p
label_offset = -50p
value       = {tick}
</tick>''')

    # tick_txt_list.append(f'''
    # <tick>
    # position    = {','.join(tick_dict[tick])}
    # # label       = {tick}
    # type        = text
    # color       = black
    # label_font  = glyph
    # size        = 0
    # thickness   = 0
    # show_label  = yes
    # label_size  = 20p
    # label_offset = 5p
    # value       = {tick}
    # </tick>''')

tick_txt = '\n'.join(tick_txt_list)

# --- make circos.conf ---
if len(accessions) == 2:
    circos_text = circos_template.render(
        accessions=';'.join(accessions),
        reversed_accession=accessions[1],
        spec_symbols=tick_txt
    )
else:
    circos_text = circos_template.render(
        accessions=';'.join(accessions),
        spec_symbols=tick_txt
    )

# --- make ideogram.conf ---
ideogram_text = ideogram_template.render()

# --- run circos ---
with TemporaryDirectory() as wd:
    with open(f'{wd}/ideogram.conf', 'w') as fh: fh.write(ideogram_text)
    with open(f'{wd}/circos.conf', 'w') as fh: fh.write(circos_text)
    with open(f'{wd}/karyotype.conf', 'w') as fh: fh.write(karyogram_txt)
    with open(f'{wd}/linkdata.txt', 'w') as fh: fh.write('\n'.join(link_list))
    print(os.popen(f'circos -conf {wd}/circos.conf && mv circos.svg {args.out_svg}').read())
