import sys
import re

assembly_dict = {}
skip = False
with open(sys.argv[1], 'r') as fi, open(sys.argv[2], 'w') as fo:
    for line in fi.readlines():
        if line[0] == '>':
            assembly = re.search('(?<=:)[^:]+(?=:)', line).group(0)
            assembly_nb = int(assembly_dict.get(assembly, - 1) + 1)
            assembly_dict[assembly] = assembly_nb
            skip = False
            if assembly_nb > 0:
                assembly += f'_{assembly_nb}'
                if sys.argv[3]:
                    skip = True
            line = '>' + assembly + '\n'
        if not skip:
            fo.write(line)
